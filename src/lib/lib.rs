//! Library for messing with text
//!
//! Proccesses text files and allows you to query for info about the text or generate new text using the input as a base.
use rand::distributions::WeightedIndex;
use rand::prelude::*;
use rand::Rng;

use std::cmp::Ordering;
use std::cmp::Reverse;
use std::collections::BinaryHeap;
use std::collections::HashMap;
use std::collections::VecDeque;

/// This struct hold basic information about the a word.
#[derive(Debug)]
struct Word {
    count: u32,
    terminal: bool,
    following: HashMap<String, Word>,
}

impl Word {
    /// Creates a new word.
    ///
    /// Each word starts with a count of one and no following words.
    fn new() -> Self {
        Word {
            count: 1,
            terminal: false,
            following: HashMap::new(),
        }
    }
    /// Add a word to the following map of a word.
    ///
    /// word_list contains the path to where in the map to store the new word which is the last word in the list.
    fn add_following_word(&mut self, mut word_list: VecDeque<&str>) {
        if word_list.len() == 1 {
            self.following
                .entry(
                    word_list
                        .pop_back()
                        .unwrap()
                        .to_string()
                        .trim_matches(&['\'', '’'][..])
                        .to_ascii_lowercase(),
                )
                .and_modify(|e| e.count += 1)
                .or_insert_with(Word::new);
        } else {
            self.following
                .get_mut(
                    &word_list
                        .pop_back()
                        .unwrap()
                        .trim_matches(&['\'', '’'][..])
                        .to_ascii_lowercase(),
                )
                .unwrap()
                .add_following_word(word_list);
        }
    }
    /// Gets a random following at the location indicated by list.
    fn get_random_sub_value(&self, mut list: VecDeque<&str>) -> Option<String> {
        //println!("test1");
        if list.is_empty() {
            let keys: Vec<&str> = self.following.keys().map(|x| x.as_str()).collect();
            //println!("test1");
            if keys.is_empty() {
                return None;
            }
            return Some(
                keys.get(rand::thread_rng().gen_range(0..keys.len()))?
                    .to_string(),
            );
        }
        //println!("test2");
        self.following
            .get(list.pop_front().unwrap())?
            .get_random_sub_value(list)
    }
    /// Same as get_random_sub_value but uses the words frequency count as a weight to the random.
    fn get_weighted_random_sub_value(&self, mut list: VecDeque<&str>) -> Option<String> {
        //println!("test1");
        if list.is_empty() {
            let map: Vec<(&str, u32)> = self
                .following
                .iter()
                .map(|(name, word)| (name.as_str(), word.count))
                .collect();
            let weight: Vec<u32> = map.iter().map(|(_, count)| *count).collect();
            //println!("test1");
            if map.is_empty() {
                return None;
            }
            return Some(
                map.get(
                    WeightedIndex::new(&weight)
                        .unwrap()
                        .sample(&mut rand::thread_rng()),
                )
                .unwrap()
                .0
                .to_string(),
            );
        }
        //println!("test2");
        self.following
            .get(list.pop_front().unwrap())?
            .get_weighted_random_sub_value(list)
    }
}

impl Eq for Word {}

impl Ord for Word {
    fn cmp(&self, other: &Self) -> Ordering {
        self.count.cmp(&other.count)
    }
}
impl PartialOrd for Word {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
impl PartialEq for Word {
    fn eq(&self, other: &Self) -> bool {
        self.count == other.count
    }
}
#[derive(Debug)]
pub struct WordMap {
    map: HashMap<String, Word>,
}

impl WordMap {
    #[allow(dead_code)]
    pub fn new() -> Self {
        WordMap {
            map: HashMap::new(),
        }
    }
    //TODO: Handle words that are connected by hyphen e.g. 'in-coming'
    //TODO: Handle Honorifics e.g. 'mrs.'
    /// Takes a string of a source text and inserts into the map.
    ///
    /// This method also takes max_depth that limits how deep the following words map will go.
    /// Making this value too large will consume large amount of memory.BinaryHeap
    ///
    /// # Examples
    ///
    ///  ```
    /// use legalzinc::WordMap;
    ///
    /// let mut map = WordMap::new();
    /// let contents = "this is a test.";
    /// map.parse(contents, 4);
    /// ```
    pub fn parse(&mut self, words: &str, max_depth: u32) {
        //split the book into chunks of related words
        let sentence_iter = words.split(
            &[
                '.', '?', '!', ';', ':', '"', '”', '“', '(', ')', '[', ']', '{', '}',
            ][..],
        );
        for sentence in sentence_iter {
            //reference of all previous words in this sentence
            let mut previous_words: VecDeque<&str> = VecDeque::new();
            //split the individual words up
            let words_iter = sentence.split(
                &[
                    ' ', '\t', '\n', '\r', ',', '%', '&', '/', '\\', '=', '+', '-', '_', '-', '—',
                ][..],
            );
            for word in words_iter {
                //if this word is nonexistent, skip it
                if word.is_empty() {
                    continue;
                }
                let _inserted_word = self
                    .map
                    //get the entry of the current word
                    .entry(
                        word.trim_matches(&['\'', '’'][..])
                            .to_string()
                            .to_ascii_lowercase(),
                    )
                    //if it exists increment the counter by one
                    .and_modify(|e| e.count += 1)
                    //if it doesn't exist create a new word
                    .or_insert_with(Word::new);

                let mut prev_into: VecDeque<&str> = VecDeque::new();

                while previous_words.len() > max_depth as usize {
                    previous_words.pop_front();
                }

                //loop though all previous words in the sentence starting with the most recent words
                for prev_word in previous_words.iter_mut().rev() {
                    prev_into.push_back(prev_word);
                    if !prev_into.is_empty() {
                        let mut sent = prev_into.clone();
                        sent.pop_back();
                        sent.push_front(word);

                        self.map
                            .get_mut(
                                &prev_word
                                    .to_string()
                                    .trim_matches(&['\'', '’'][..])
                                    .to_ascii_lowercase(),
                            )
                            .unwrap()
                            .add_following_word(sent);
                    }
                }

                previous_words.push_back(word);
            }

            if let Some(last_word) = previous_words.pop_back() {
                self.map
                    .entry(last_word.to_string())
                    .and_modify(|x| x.terminal = true);
            }
        }
    }
    /// Gets a random values from the WordMap.
    pub fn get_random_value(&self) -> String {
        let keys: Vec<&str> = self.map.keys().map(|x| x.as_str()).collect();
        keys.get(rand::thread_rng().gen_range(0..keys.len()))
            .unwrap()
            .to_string()
    }
    /// Same as get_random_value but is weighted by word frequency
    pub fn get_weighted_random_value(&self) -> String {
        let map: Vec<(&str, u32)> = self
            .map
            .iter()
            .map(|(name, word)| (name.as_str(), word.count))
            .collect();
        let weight: Vec<u32> = map.iter().map(|(_, count)| *count).collect();
        map.get(
            WeightedIndex::new(&weight)
                .unwrap()
                .sample(&mut rand::thread_rng()),
        )
        .unwrap()
        .0
        .to_string()
    }
    /// Gets a random following word from the map.
    /// Take a list that contains directions to the correct list of following words
    pub fn get_random_sub_value(&self, mut list: VecDeque<&str>) -> Option<String> {
        self.map.get(list.pop_front()?)?.get_random_sub_value(list)
    }
    /// Same as get_random_sub_value but is weighted by word frequency
    pub fn get_weighted_random_sub_value(&self, mut list: VecDeque<&str>) -> Option<String> {
        self.map
            .get(list.pop_front()?)?
            .get_weighted_random_sub_value(list)
    }

    /// Serializes Map into a file
  

    /// Deserializes Map from a file
   
    /// Returns a string of words created by a markov chain
    ///
    /// Takes a length argument that is the prefered length of the sentence, but the output might be shorter or longer that this value.
    ///
    /// The returned string may have fewer words if the chain encounters a word that only apears at the end of sentences in the training data.
    /// The chain cannot continue because it cannot predict what word would come next.
    ///  
    /// The returned string may have more words because the chain will only end on a word it has seen occur at the end of a sentence in the training data.
    ///
    /// Takes a depth argument that controls how long of a chain the program will attempt to create. If a chain of given length is not possible
    /// i.e. there are not following words found, the depth will be reduced by one and attempted again. if depth becomes zero the method will terminate.
    pub fn markov_chain(&self, len: u32, depth: u32) -> String {
        let mut output = String::new();

        let val = self.get_random_value();
        output.push_str(val.as_str());
        output.push(' ');

        let mut previous_vals: VecDeque<String> = VecDeque::new();
        previous_vals.push_back(val.clone());
        let mut index = 0;
        loop {
            while previous_vals.len() > depth as usize {
                previous_vals.pop_front();
            }

            if let Some(word) =
                self.get_random_sub_value(previous_vals.iter().map(|x| x.as_str()).collect())
            {
                output.push_str(word.as_str());
                if index > len {
                    if self.map.get(&word).unwrap().terminal {
                        output.push('.');
                        break;
                    } else {
                        output.push(' ');
                    }
                } else {
                    output.push(' ');
                }
                previous_vals.push_back(word.clone());
            } else {
                println!("\ncouldn't find word: {:?}", previous_vals);
                previous_vals.pop_front();
                if previous_vals.is_empty(){
                    break;
                }
            }
            index += 1;
        }
        output
    }
    /// Same as markov_chain but weighted by word frequency.
    pub fn weighted_markov_chain(&self, len: u32, depth: u32) -> String {
        let mut output = String::new();

        let val = self.get_weighted_random_value();
        output.push_str(val.as_str());
        output.push(' ');

        let mut previous_vals: VecDeque<String> = VecDeque::new();
        previous_vals.push_back(val.clone());
        let mut index = 0;
        loop {
            while previous_vals.len() > depth as usize {
                previous_vals.pop_front();
            }

            if let Some(word) = self
                .get_weighted_random_sub_value(previous_vals.iter().map(|x| x.as_str()).collect())
            {
                output.push_str(word.as_str());
                if index > len {
                    if self.map.get(&word).unwrap().terminal || previous_vals.is_empty() {
                        output.push('.');
                        break;
                    } else {
                        output.push(' ');
                    }
                } else {
                    output.push(' ');
                }
                previous_vals.push_back(word.clone());
            } else {
                println!("\ncouldn't find word: {:?}", previous_vals);
                previous_vals.pop_front();
                
                if previous_vals.is_empty(){
                    break;
                }
            }

            index += 1;
        }
        output
    }
    /// Prints all words on the map from least frequent to most frequent.
    pub fn print_most_used_words(&self) {
        let mut list: BinaryHeap<_> = self
            .map
            .iter()
            .map(|(k, v)| (&v.count, k.as_str()))
            .map(Reverse)
            .collect();
        while let Some(Reverse((count, word))) = list.pop() {
            println!("{}:{}", word, count);
        }
    }
}
